# Smart badge library

The colours for the gradients of the default badge are:
## Green
Start: 3eb810
Stop: 2f830e

## Orange
Start: dab226
Stop: c9a115

## Red
Start: d9634d
Stop: c3543e
